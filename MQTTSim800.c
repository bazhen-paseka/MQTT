/*
 * MQTTSim800.c
 *
 *  Created on: Jan 4, 2020
 *      Author: Bulanov Konstantin
 *
 *  Contact information
 *  -------------------
 *
 * e-mail   :   leech001@gmail.com
 * telegram :   https://t.me/leech001
 *
 */


/*
 * -----------------------------------------------------------------------------------------------------------------------------------------------
           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2020 Bulanov Konstantin <leech001@gmail.com>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.

  MQTT packet https://github.com/eclipse/paho.mqtt.embedded-c/tree/master/MQTTPacket
 * ------------------------------------------------------------------------------------------------------------------------------------------------
*/

#include "MQTTSim800.h"
#include "main.h"
#include "usart.h"
#include <string.h>
#include "MQTTPacket.h"
//**********************************************************************************************

#if FREERTOS == 1
#include <cmsis_os.h>
#endif

//extern SIM800_t SIM800;
//**********************************************************************************************

typedef enum
{
    STEP_FIRST				,
	WAIT_FIRST_DELAY		,
    STEP_MUX_OFF			,
	WAIT_MUX_DELAY			,
    STEP_GSM_OFF			,
    WAIT_GSM_DELAY			,
    STEP_START_MQTT_INIT	,
	WAIT_START_MQTT_INIT	,
    STEP_SEND_AT_FIRST		,
    WAIY_DELAY_AT_FIRST		,
	STEP_SEND_ATE0			,
	WAIT_DELAY_ATE0			,
	STEP_SEND_CIPSHUT		,
	WAIT_DELAY_CIPSHUT		,
	STEP_SEND_CGATT			,
	WAIT_DELAY_CGATT		,
	STEP_SEND_CIPMODE		,
	WAIT_DELAY_CIPMODE		,
	STEP_SEND_COPS			,
	WAIT_DELAY_COPS			,
	STEP_SEND_CSTT_1		,
	WAIT_DELAY_CSTT_1		,
	STEP_SEND_CSTT_2		,
	WAIT_DELAY_CSTT_2		,
	STEP_SEND_CIICR			,
	WAIT_DELAY_CIICR		,
	STEP_SEND_CIFSR			,
	WAIT_DELAY_CIFSR		,
	STEP_CHECK_INIT_ERRORS	,
	STEP_SEND_CIPSTARS		,
	WAIT_DELAY_CIPSTARS		,
	STEP_SEND_DATAS			,
	WAIT_DELAY_DATAS		,
	STEP_CHECK_CONNECT		,
	STEP_WRONG_CONNECT		,
	STEP_GOOD_CONNECT		,
	STEP_WAIT_NEXT_MESS
} SIM800_steps_enum;

	/*
		//#define ATI "ATI" // название и версия модуля

		set_comand(ATE);     // отключить «эхо»
		set_comand("AT+CPAS");  // проверка статуса модема +CPAS:
																	0 — готов к работе,
																	2 — неизвестно,
																	3 — входящий звонок,
																	4 — в режиме соединения.

		set_comand("AT+CREG?");  // проверка регистрации в сети - должен вернуть  +CREG: 0,1 краще 0,5
																			1 - зерегистрированно,
																			2 - не зарегистрированно идет поиск сети,
																			3 - регистрация отклонена,
																			4 - неизвестно,
																			5 - зарегистрированно роуминг
		set_comand("AT+CGATT=1");         // включить GPRS сервис/ "AT+CGATT=0" // отключить GPRS сервис (ждать)
		set_comand("AT+CGATT?");          // проверить подключен ли к GPRS - +CGATT: 1-Ok
		set_comand("AT+CIPMUX=0");        // установить режим одиночного соединения
		set_comand(ATCIICR);          // устанавливаем беспроводное подключение GPRS
		set_comand( "AT+CGSN"  ) ;        		//  IMEI модуля
		set_comand( "AT+CSPN?" ) ;				//	operator
		set_comand( "AT+COPS?" ) ;				// в какой сети зарегистрирован
		set_comand( "AT+CSQ"  ) ;				//	информация о качестве сигнала
 	 */

//**********************************************************

	#define 	MQTT_BUFFER_SIZE	1460
	uint8_t 	rx_buffer	[MQTT_BUFFER_SIZE] = { 0, } ;
	char 		mqtt_buffer	[MQTT_BUFFER_SIZE] = { 0, } ;

	uint8_t 	rx_data 		= 0 ;
	uint16_t 	rx_index 		= 0 ;
	uint8_t 	mqtt_receive 	= 0 ;
	uint16_t 	mqtt_index 		= 0 ;
	uint32_t 	system_tik_u32 	= 0 ;

	int 		message_nmr 	= 0 ;
	int 		message_cnt_i 	= 1	;
    uint8_t 	mqtt_sub_flag 	= 0	;
    uint32_t	check_network_cnt = 0 ;


	char		debug_buffer	[0xFF] = { 0, } ;

	SIM800_t		 	SIM800							;
	SIM800_steps_enum	sim800_current_state = STEP_FIRST ;

#ifdef COMP
	char *device_name = "c0mp" ;
#else
	char *device_name = "n0ut" ;
#endif

	char 	*topic_name 	= "korablik" ;
    int 	cmd_errors_i 	= 0 ;

    #define SEND_SIZE					128
    char 	command_string[SEND_SIZE] 	= {0} ;
	char 	reply_string  [SEND_SIZE]  	= {0} ;

//**********************************************************************************************

void RF_sim800_Init(void) {
	sprintf( debug_buffer, "\r\n\r\n\t Start:\r\n"  ) ;
	HAL_UART_Transmit( DEBUG_UART, (uint8_t *)debug_buffer, strlen(debug_buffer), 1000 );

	int soft_version_arr_int[3];
	soft_version_arr_int[0] = ((SOFT_VERSION) / 100) %10 ;
	soft_version_arr_int[1] = ((SOFT_VERSION) /  10) %10 ;
	soft_version_arr_int[2] = ((SOFT_VERSION)      ) %10 ;

	sprintf(debug_buffer,"\t Ver: v%d.%d.%d\r\n" ,
			soft_version_arr_int[0] , soft_version_arr_int[1] , soft_version_arr_int[2] ) ;
	HAL_UART_Transmit( DEBUG_UART, (uint8_t *)debug_buffer , strlen(debug_buffer) , 1000 ) ;

	sprintf(debug_buffer,"\t CubeMX Ver: %d\r\n", CUBEMX_VERSION);
	HAL_UART_Transmit( DEBUG_UART, (uint8_t *)debug_buffer , strlen(debug_buffer) , 1000 ) ;

	#define 	DATE_as_int_str 	(__DATE__)
	#define 	TIME_as_int_str 	(__TIME__)
	sprintf(debug_buffer,"\t build: %s,  time: %s. \r\n" , DATE_as_int_str , TIME_as_int_str ) ;
	HAL_UART_Transmit( DEBUG_UART, (uint8_t *)debug_buffer , strlen(debug_buffer) , 1000 ) ;

	sprintf(debug_buffer,"\t MQTT + SIM800c + stm32-G081-RBT6 \r\n"  ) ;
	HAL_UART_Transmit( DEBUG_UART, (uint8_t *)debug_buffer , strlen(debug_buffer) , 1000 ) ;

	sprintf( debug_buffer, "\t for Debug: UART1(RC, column#2), 115200, 8, noPatiry, 1 \r\n" ) ;
	HAL_UART_Transmit(DEBUG_UART, (uint8_t *)debug_buffer, strlen(debug_buffer), 1000 );

	HAL_GPIO_WritePin( LED_GPIO_Port,  LED_Pin, GPIO_PIN_RESET);
	for (int i=0; i<7; i++) {
		sprintf(debug_buffer,"TogglePin\r\n"  ) ;
		HAL_UART_Transmit( DEBUG_UART, (uint8_t *)debug_buffer , strlen(debug_buffer) , 1000 ) ;
		HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
		HAL_Delay(1000);
	}
	HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_RESET);

#ifdef LIFECELL
		SIM800.sim.apn = "internet";	//	for lifecell
#else
	 	SIM800.sim.apn = "internet.emt.ee";
#endif

	    SIM800.sim.apn_user = "";
	    SIM800.sim.apn_pass = "";
	    SIM800.mqttServer.host = "broker.emqx.io";
	    SIM800.mqttServer.port = 1883;
	    SIM800.mqttClient.username = "runferry";	//	з`їдає останній символ - ?
	    SIM800.mqttClient.pass = "password";
	    SIM800.mqttClient.keepAliveInterval = 120;
	#ifdef COMP
	    SIM800.mqttClient.clientID = "TestSubComp";	//	з`їдає останній символ - ?
	#else
	    SIM800.mqttClient.clientID = "TestSubNout";
	#endif

	sim800_current_state = STEP_FIRST ;
}
//**********************************************************************************************

void RF_sim800_Main(void) {
	switch (sim800_current_state) {
		case STEP_FIRST : {
			sprintf( debug_buffer, "MQTT device name: \"%s\"\r\n" , device_name ) ;
			HAL_UART_Transmit(DEBUG_UART, (uint8_t *)debug_buffer, strlen(debug_buffer), 1000 );

			system_tik_u32 = HAL_GetTick();
			sim800_current_state = WAIT_FIRST_DELAY ;
		} break ;

		case WAIT_FIRST_DELAY : {
			if ( (HAL_GetTick() - system_tik_u32) < CMD_DELAY ) {
				sim800_current_state = WAIT_FIRST_DELAY ;
				break ;
			} else {
				sim800_current_state = STEP_MUX_OFF ;
			}
		} break ;

		case STEP_MUX_OFF : {
			sprintf( debug_buffer, "MUX_pin -> 0; GSM_Pin -> 1, wait  %d sec ...\r\n" , MUX_DELAY/1000) ;
			HAL_UART_Transmit(DEBUG_UART, (uint8_t *)debug_buffer, strlen(debug_buffer), 1000 );

			HAL_GPIO_WritePin(GSM_MUX_GPIO_Port, GSM_MUX_Pin, 0);
			HAL_GPIO_WritePin(GSM_ON_OFF_GPIO_Port, GSM_ON_OFF_Pin, 1);

			system_tik_u32 = HAL_GetTick();
			sim800_current_state = WAIT_MUX_DELAY ;
		} break ;

		case WAIT_MUX_DELAY : {
			if ( (HAL_GetTick() - system_tik_u32) < MUX_DELAY ) {
				sim800_current_state = WAIT_MUX_DELAY ;
				break ;
			} else {
				sim800_current_state = STEP_GSM_OFF ;
			}
		} break ;

		case STEP_GSM_OFF : {
			sprintf( debug_buffer, "MUX_pin -> 0; GSM_Pin -> 0, wait %d sec ...\r\n", GSM_DELAY/1000 ) ;
			HAL_UART_Transmit(DEBUG_UART, (uint8_t *)debug_buffer, strlen(debug_buffer), 1000 );

			HAL_GPIO_WritePin(GSM_ON_OFF_GPIO_Port, GSM_ON_OFF_Pin, 0);

			system_tik_u32 = HAL_GetTick();
			sim800_current_state = WAIT_GSM_DELAY ;
		} break ;

		case WAIT_GSM_DELAY : {
			if ( (HAL_GetTick() - system_tik_u32) < GSM_DELAY ) {
				sim800_current_state = WAIT_GSM_DELAY ;
				break ;
			} else {
				sim800_current_state = STEP_START_MQTT_INIT ;
			}
		} break ;

		case STEP_START_MQTT_INIT : {
		    SIM800.mqttServer.connect = 0;
		    cmd_errors_i = 0;
		    HAL_UART_Receive_IT(SIM800_UART, &rx_data, 1);
			system_tik_u32 = HAL_GetTick();
			sim800_current_state = WAIT_START_MQTT_INIT ;
		} break ;

		case WAIT_START_MQTT_INIT: {
			if ( (HAL_GetTick() - system_tik_u32) < CMD_DELAY ) {
				sim800_current_state = WAIT_START_MQTT_INIT ;
				break ;
			} else {
				sim800_current_state = STEP_SEND_AT_FIRST ;
				break ;
			}
		} break ;

		case STEP_SEND_AT_FIRST : {
			sprintf( command_string,	"%s",  "AT\r\n" 	) ;	//	Если модуль успешно стартовал, то отвечает "OK"
		    sprintf( reply_string,		"%s",  "AT\r\r\n"   ) ;
			HAL_UART_Transmit(SIM800_UART, (unsigned char *) command_string, (uint16_t) strlen(command_string), 1000 );
			system_tik_u32 = HAL_GetTick();
			sim800_current_state = WAIY_DELAY_AT_FIRST ;
		} break ;

		case WAIY_DELAY_AT_FIRST: {
			if ( (HAL_GetTick() - system_tik_u32) < CMD_DELAY ) {
				sim800_current_state = WAIY_DELAY_AT_FIRST ;
				break ;
			} else {
				if (strstr(mqtt_buffer, reply_string) != NULL) {
					sprintf( debug_buffer, " + %s", command_string ) ;
				} else {
					sprintf( debug_buffer, " - %s", command_string ) ;
					// Error
				}
				HAL_UART_Transmit(DEBUG_UART, (uint8_t *)debug_buffer, strlen(debug_buffer), 1000 );
				sim800_current_state = STEP_SEND_ATE0 ;
				clearRxBuffer();
			}
		} break ;

		case STEP_SEND_ATE0 : {
			sprintf( command_string,	"%s",  "ATE0\r\n"	) ;	//включить/выключить эхо
		    sprintf( reply_string,		"%s",  "ATE0\r\r\n"	) ;
			HAL_UART_Transmit(SIM800_UART, (unsigned char *) command_string, (uint16_t) strlen(command_string), 1000 );
			system_tik_u32 = HAL_GetTick();
			sim800_current_state = WAIT_DELAY_ATE0 ;
		} break ;

		case WAIT_DELAY_ATE0 : {
			if ( (HAL_GetTick() - system_tik_u32) < CMD_DELAY ) {
				sim800_current_state = WAIT_DELAY_ATE0 ;
				break ;
			} else {
				if (strstr(mqtt_buffer, reply_string) != NULL) {
					sprintf( debug_buffer, " + %s", command_string ) ;
				} else {
					sprintf( debug_buffer, " - %s", command_string ) ;
					// Error
				}
				HAL_UART_Transmit(DEBUG_UART, (uint8_t *)debug_buffer, strlen(debug_buffer), 1000 );
				sim800_current_state = STEP_SEND_CIPSHUT ;
				clearRxBuffer();
			}
		} break ;
		//-----------------------------------------------------------

		case STEP_SEND_CIPSHUT : {	   // разорвать все соединения и деактивировать интерфейс GPRS
			sprintf( command_string,	"%s",  "AT+CIPSHUT\r\n" ) ;
		    sprintf( reply_string,		"%s",  "SHUT OK\r\n"   ) ;
			HAL_UART_Transmit(SIM800_UART, (unsigned char *) command_string, (uint16_t) strlen(command_string), 1000 );
			system_tik_u32 = HAL_GetTick();
			sim800_current_state = WAIT_DELAY_CIPSHUT ;
		} break ;

		case WAIT_DELAY_CIPSHUT : {
			if ( (HAL_GetTick() - system_tik_u32) < CMD_DELAY ) {
				sim800_current_state = WAIT_DELAY_CIPSHUT ;
				break ;
			} else {
				if (strstr(mqtt_buffer, reply_string) != NULL) {
					sprintf( debug_buffer, " + %s", command_string ) ;
				} else {
					sprintf( debug_buffer, " - %s", command_string) ;
					cmd_errors_i++ ;
				}
				sim800_current_state = STEP_SEND_CGATT ;
				clearRxBuffer();
				HAL_UART_Transmit(DEBUG_UART, (uint8_t *)debug_buffer, strlen(debug_buffer), 1000 );
			}
		} break ;
		//-----------------------------------------------------------

		case STEP_SEND_CGATT : {	// включить GPRS сервис (ждать)
			sprintf( command_string,	"%s",  "AT+CGATT=1\r\n" ) ;
		    sprintf( reply_string,		"%s",  "OK\r\n"   ) ;
			HAL_UART_Transmit(SIM800_UART, (unsigned char *) command_string, (uint16_t) strlen(command_string), 1000 );
			system_tik_u32 = HAL_GetTick();
			sim800_current_state = WAIT_DELAY_CGATT ;
		} break ;

		case WAIT_DELAY_CGATT : {
			if ( (HAL_GetTick() - system_tik_u32) < CMD_DELAY ) {
				sim800_current_state = WAIT_DELAY_CGATT ;
				break ;
			} else {
				if (strstr(mqtt_buffer, reply_string) != NULL) {
					sprintf( debug_buffer, " + %s", command_string ) ;
				} else {
					sprintf( debug_buffer, " - %s", command_string) ;
					cmd_errors_i++ ;
				}
				sim800_current_state = STEP_SEND_CIPMODE ;
				clearRxBuffer();
				HAL_UART_Transmit(DEBUG_UART, (uint8_t *)debug_buffer, strlen(debug_buffer), 1000 );
			}
		} break ;
		//-----------------------------------------------------------

		case STEP_SEND_CIPMODE : {	//	Установить сквозной режим "unvarnished transmission mode"
									//	TCP/IP	AT+CIPMODE=<режим>	AT+CIPMODE?	0=обычный режим,
									//	1=unvarnished transmission mode (режим 1 доступен только при AT+CIPMUX=0).
		    sprintf( command_string,	"%s",  "AT+CIPMODE=1\r\n" ) ;
		    sprintf( reply_string,		"%s",  "OK\r\n"   ) ;
			HAL_UART_Transmit(SIM800_UART, (unsigned char *) command_string, (uint16_t) strlen(command_string), 1000 );
			system_tik_u32 = HAL_GetTick();
			sim800_current_state = WAIT_DELAY_CIPMODE ;
		} break ;

		case WAIT_DELAY_CIPMODE : {
			if ( (HAL_GetTick() - system_tik_u32) < CMD_DELAY ) {
				sim800_current_state = WAIT_DELAY_CIPMODE ;
				break ;
			} else {
				if (strstr(mqtt_buffer, reply_string) != NULL) {
					sprintf( debug_buffer, " + %s", command_string ) ;
				} else {
					sprintf( debug_buffer, " - %s", command_string) ;
					cmd_errors_i++ ;
				}
				sim800_current_state = STEP_SEND_COPS ;
				clearRxBuffer();
				HAL_UART_Transmit(DEBUG_UART, (uint8_t *)debug_buffer, strlen(debug_buffer), 1000 );
			}
		} break ;
		//-----------------------------------------------------------

		case STEP_SEND_COPS : {	// в какой сети зарегистрирован
		    sprintf( command_string,	"%s",  "AT+COPS?\r\n" ) ;
		    sprintf( reply_string,		"%s",  "0,0,\"Astelit\""   ) ;
			HAL_UART_Transmit(SIM800_UART, (unsigned char *) command_string, (uint16_t) strlen(command_string), 1000 );
			system_tik_u32 = HAL_GetTick();
			sim800_current_state = WAIT_DELAY_COPS ;
		} break ;

		case WAIT_DELAY_COPS : {
			if ( (HAL_GetTick() - system_tik_u32) < CMD_DELAY ) {
				sim800_current_state = WAIT_DELAY_COPS ;
				break ;
			} else {
				#define NETWORK_NAME_SIZE	30
				char network_name_str[NETWORK_NAME_SIZE] = { 0, } ;
				memcpy( network_name_str, mqtt_buffer,  NETWORK_NAME_SIZE + 1 );
		        sprintf( debug_buffer, "%d) %s", (int)(system_tik_u32/1000), network_name_str ) ;
				HAL_UART_Transmit(DEBUG_UART, (uint8_t *)debug_buffer, strlen(debug_buffer), 1000 );
				clearRxBuffer();
				check_network_cnt++;
				if ( check_network_cnt < 10 ) {
					sim800_current_state = STEP_SEND_COPS ;
				} else {
					sim800_current_state = STEP_SEND_CSTT_1 ;
				}
			}
		} break ;
		//-----------------------------------------------------------

		case STEP_SEND_CSTT_1 : {
			char str[32] = {0};
			if (SIM800.sim.apn[0] && SIM800.sim.apn_user[0] && SIM800.sim.apn_pass[0]) {
				snprintf(str, sizeof(str), "AT+CSTT=\"%s\",\"%s\",\"%s\"\r\n", SIM800.sim.apn, SIM800.sim.apn_user, SIM800.sim.apn_pass);
			} else if (SIM800.sim.apn[0]) {
				snprintf(str, sizeof(str), "AT+CSTT=\"%s\"\r\n", SIM800.sim.apn);
			} else {
				cmd_errors_i += 1;
				sprintf( debug_buffer, "STEP_SEND_CSTT_1 ERROR\r\n") ;
				HAL_UART_Transmit(DEBUG_UART, (uint8_t *)debug_buffer, strlen(debug_buffer), 1000 );
				//return error;
			}
			sprintf( command_string,	"%s",  str ) ;
		    sprintf( reply_string,		"%s",  "OK\r\n"   ) ;
			HAL_UART_Transmit( SIM800_UART, (unsigned char *) command_string, (uint16_t) strlen(command_string), 1000 );
			system_tik_u32 = HAL_GetTick();
			sim800_current_state = WAIT_DELAY_CSTT_1 ;
		} break ;

		case WAIT_DELAY_CSTT_1 : {
			if ( (HAL_GetTick() - system_tik_u32) < CMD_DELAY ) {
				sim800_current_state = WAIT_DELAY_CSTT_1 ;
				break ;
			} else {
				if (strstr(mqtt_buffer, reply_string) != NULL) {
					sprintf( debug_buffer, " + %s", command_string ) ;
				} else {
					sprintf( debug_buffer, " - %s", command_string) ;
					cmd_errors_i++ ;
				}
				sim800_current_state = STEP_SEND_CSTT_2 ;
				clearRxBuffer();
				HAL_UART_Transmit(DEBUG_UART, (uint8_t *)debug_buffer, strlen(debug_buffer), 1000 );
			}
		} break ;
		//-----------------------------------------------------------

		case STEP_SEND_CSTT_2 : {
			sprintf( command_string,	"%s",  "AT+CSTT?\r\n" ) ;
		    sprintf( reply_string,		"%s",  ""   ) ;
			HAL_UART_Transmit(SIM800_UART, (unsigned char *) command_string, (uint16_t) strlen(command_string), 1000 );
			system_tik_u32 = HAL_GetTick();
			sim800_current_state = WAIT_DELAY_CSTT_2 ;
		} break ;

		case WAIT_DELAY_CSTT_2 : {
			if ( (HAL_GetTick() - system_tik_u32) < CMD_DELAY ) {
				sim800_current_state = WAIT_DELAY_CSTT_2 ;
				break ;
			} else {
				if (strstr(mqtt_buffer, reply_string) != NULL) {
					sprintf( debug_buffer, " + %s", command_string ) ;
				} else {
					sprintf( debug_buffer, " - %s", command_string) ;
					cmd_errors_i++ ;
				}
				sim800_current_state = STEP_SEND_CIICR ;
				clearRxBuffer();
				HAL_UART_Transmit(DEBUG_UART, (uint8_t *)debug_buffer, strlen(debug_buffer), 1000 );
			}
		} break ;
		//-----------------------------------------------------------

		case STEP_SEND_CIICR : {
			sprintf( command_string,	"%s",  "AT+CIICR\r\n" ) ;
		    sprintf( reply_string,		"%s",  "OK\r\n"   ) ;
			HAL_UART_Transmit(SIM800_UART, (unsigned char *) command_string, (uint16_t) strlen(command_string), 1000 );
			system_tik_u32 = HAL_GetTick();
			sim800_current_state = WAIT_DELAY_CIICR ;
		} break ;
		case WAIT_DELAY_CIICR : {
			if ( (HAL_GetTick() - system_tik_u32) < CMD_DELAY ) {
				sim800_current_state = WAIT_DELAY_CIICR ;
				break ;
			} else {
				if (strstr(mqtt_buffer, reply_string) != NULL) {
					sprintf( debug_buffer, " + %s", command_string ) ;
				} else {
					sprintf( debug_buffer, " - %s", command_string) ;
					cmd_errors_i++ ;
				}
				sim800_current_state = STEP_SEND_CIFSR ;
				clearRxBuffer();
				HAL_UART_Transmit(DEBUG_UART, (uint8_t *)debug_buffer, strlen(debug_buffer), 1000 );
			}
		} break ;
		//-----------------------------------------------------------

		case STEP_SEND_CIFSR : {	//	Отобразить IP адрес
			sprintf( command_string,	"%s",  "AT+CIFSR\r\n" ) ;
		    sprintf( reply_string,		"%s",  ""   ) ;
			HAL_UART_Transmit(SIM800_UART, (unsigned char *) command_string, (uint16_t) strlen(command_string), 1000 );
			system_tik_u32 = HAL_GetTick();
			sim800_current_state = WAIT_DELAY_CIFSR ;
		} break ;

		case WAIT_DELAY_CIFSR : {
			if ( (HAL_GetTick() - system_tik_u32) < CMD_DELAY ) {
				sim800_current_state = WAIT_DELAY_CIFSR ;
				break ;
			} else {
				if (strstr(mqtt_buffer, reply_string) != NULL) {
					sprintf( debug_buffer, " + %s", command_string ) ;
				} else {
					sprintf( debug_buffer, " - %s", command_string ) ;
					cmd_errors_i++ ;
				}
				sim800_current_state = STEP_CHECK_INIT_ERRORS ;
				clearRxBuffer();
				HAL_UART_Transmit(DEBUG_UART, (uint8_t *)debug_buffer, strlen(debug_buffer), 1000);
			}
		} break ;
		//-----------------------------------------------------------

		case STEP_CHECK_INIT_ERRORS : {
			sprintf( debug_buffer, "Init errors= %d;\r\n" , cmd_errors_i ) ;
			HAL_UART_Transmit(DEBUG_UART, (uint8_t *)debug_buffer, strlen(debug_buffer), 1000);

		    if ( cmd_errors_i == 0) {
		    	SIM800.mqttReceive.newEvent = 0;
		        SIM800.mqttServer.connect = 0;
		    	sim800_current_state = STEP_SEND_CIPSTARS ;
		    } else {
		    	sim800_current_state = STEP_WRONG_CONNECT ;	//	reInit
		    }
		} break ;
		//-----------------------------------------------------------

		case STEP_SEND_CIPSTARS : {	//	Установить подключение TCP
			char str[SEND_SIZE] = {0};
			sprintf(str, "AT+CIPSTART=\"TCP\",\"%s\",%d\r\n", SIM800.mqttServer.host, SIM800.mqttServer.port);
			sprintf( command_string,	"%s",  str ) ;
		    sprintf( reply_string,		"%s",  "CONNECT\r\n"   ) ;
			HAL_UART_Transmit(SIM800_UART, (unsigned char *) command_string, (uint16_t) strlen(command_string), 1000 );
			system_tik_u32 = HAL_GetTick();
			sim800_current_state = WAIT_DELAY_CIPSTARS ;
		} break ;

		case WAIT_DELAY_CIPSTARS : {
			if ( (HAL_GetTick() - system_tik_u32) < CONNECT_DELAY ) {
				sim800_current_state = WAIT_DELAY_CIPSTARS ;
				break ;
			} else {
				if (strstr(mqtt_buffer, reply_string) != NULL) {
					sprintf( debug_buffer, " + %s", command_string ) ;
				} else {
					sprintf( debug_buffer, " - %s", command_string ) ;
					cmd_errors_i++ ;
				}
				sim800_current_state = STEP_SEND_DATAS ;
				clearRxBuffer();
				HAL_UART_Transmit(DEBUG_UART, (uint8_t *)debug_buffer, strlen(debug_buffer), 1000);
			}
		} break ;
		//-----------------------------------------------------------

		case STEP_SEND_DATAS : {
		    if (SIM800.mqttServer.connect == 1) {
		    	MQTTPacket_connectData datas = MQTTPacket_connectData_initializer;
		        datas.username.cstring  = SIM800.mqttClient.username;
		        datas.password.cstring  = SIM800.mqttClient.pass;
		        datas.clientID.cstring  = SIM800.mqttClient.clientID;
		        datas.keepAliveInterval = SIM800.mqttClient.keepAliveInterval;
		        datas.cleansession = 1;
		        unsigned char client_buffer[128] = {0};
		        int mqtt_len = MQTTSerialize_connect(client_buffer, sizeof(client_buffer), &datas);
				HAL_UART_Transmit(DEBUG_UART , client_buffer, mqtt_len, 1000 ) ;
		        HAL_UART_Transmit(SIM800_UART, client_buffer, mqtt_len, 1000 ) ;
				sprintf( command_string,	"%s",  client_buffer ) ;
			    sprintf( reply_string,		"%s",  "CONNECT\r\n"   ) ;
		    	system_tik_u32 = HAL_GetTick();
		    	sim800_current_state = WAIT_DELAY_DATAS ;
		    } else {
		    	sim800_current_state = STEP_WRONG_CONNECT ;
		    }
		} break ;

		case WAIT_DELAY_DATAS : {
			if ( (HAL_GetTick() - system_tik_u32) < CONNECT_DELAY ) {
				sim800_current_state = WAIT_DELAY_DATAS ;
				break ;
			} else {
				if (strstr(mqtt_buffer, reply_string) != NULL) {
					sprintf( debug_buffer, " + %s", command_string ) ;
				} else {
					sprintf( debug_buffer, " - %s", command_string ) ;
					cmd_errors_i++ ;
				}
				HAL_UART_Transmit(DEBUG_UART, (uint8_t *)debug_buffer, strlen(debug_buffer), 1000);
				sim800_current_state = STEP_CHECK_CONNECT ;
				clearRxBuffer();
			}
		} break ;
		//-----------------------------------------------------------

		case STEP_CHECK_CONNECT : {
			if (SIM800.mqttServer.connect == 0) {
				sprintf( debug_buffer, "Wrong connect!\r\n" ) ;
				HAL_UART_Transmit(DEBUG_UART, (uint8_t *)debug_buffer, strlen(debug_buffer), 1000);
				mqtt_sub_flag = 0;
				message_cnt_i = 0;
				system_tik_u32 = HAL_GetTick();
				sim800_current_state = STEP_WRONG_CONNECT ;
			} else {
				sim800_current_state = STEP_GOOD_CONNECT ;
			}
		} break ;

		case STEP_WRONG_CONNECT : {
			if ( (HAL_GetTick() - system_tik_u32) < CMD_DELAY ) {
				sim800_current_state = STEP_WRONG_CONNECT ;
				break ;
			} else {
				sim800_current_state = STEP_START_MQTT_INIT ;
			}
		} break ;

		case STEP_GOOD_CONNECT : {
			if (SIM800.mqttServer.connect == 1) {
				if (mqtt_sub_flag == 0) {
					mqtt_sub_flag = 1;
					sprintf( debug_buffer, "\r\nConnected successfully. MQTT_Sub: \"%s\"\r\n", topic_name ) ;
					HAL_UART_Transmit(DEBUG_UART, (uint8_t *)debug_buffer, strlen(debug_buffer), 1000 );
					MQTT_Sub(topic_name);
					system_tik_u32 = HAL_GetTick();
					sim800_current_state = STEP_WAIT_NEXT_MESS ;
					break ;		// замысть add Delay 5000
				}

				if (SIM800.mqttReceive.newEvent) {
					unsigned char *topic = SIM800.mqttReceive.topic;
					unsigned char *payload = SIM800.mqttReceive.payload ;
					int payloadLen = SIM800.mqttReceive.payloadLen;
					sprintf( debug_buffer, "\tRX:\t%s\t%s\tlen:%d \r\n", topic , payload , payloadLen ) ;
					HAL_UART_Transmit(DEBUG_UART, (uint8_t *)debug_buffer, strlen(debug_buffer), 1000 );
					SIM800.mqttReceive.newEvent = 0;
				}

				message_cnt_i++;
				if (message_cnt_i%6 == 0) {
					sprintf( debug_buffer, "\r\n\tSend:") ;
					HAL_UART_Transmit(DEBUG_UART, (uint8_t *)debug_buffer, strlen(debug_buffer), 1000 );

					sprintf( debug_buffer, "%s-%04d", device_name, message_nmr++) ;
					MQTT_Pub(topic_name, debug_buffer);											//			!!!!!! MQTT_Pub

					sprintf( debug_buffer, "\t%s\t%s-%04d", topic_name , device_name, message_nmr-1 ) ;
					HAL_UART_Transmit(DEBUG_UART, (uint8_t *)debug_buffer, strlen(debug_buffer), 1000 );

					sprintf( debug_buffer, "\r\n") ;
					HAL_UART_Transmit(DEBUG_UART, (uint8_t *)debug_buffer, strlen(debug_buffer), 1000 );
				}
				sprintf( debug_buffer, ".") ;
				HAL_UART_Transmit(DEBUG_UART, (uint8_t *)debug_buffer, strlen(debug_buffer), 1000 );
				system_tik_u32 = HAL_GetTick();
				sim800_current_state = STEP_WAIT_NEXT_MESS ;
			} else {
				sprintf( debug_buffer, "connect=0 -> ReInit\r\n\r\n") ;
				HAL_UART_Transmit(DEBUG_UART, (uint8_t *)debug_buffer, strlen(debug_buffer), 1000 );
				system_tik_u32 = HAL_GetTick();
				sim800_current_state = STEP_WRONG_CONNECT ;
			}
		} break ;
		//-----------------------------------------------------------


		case STEP_WAIT_NEXT_MESS : {
			if ( (HAL_GetTick() - system_tik_u32) < CMD_DELAY ) {
				sim800_current_state = STEP_WAIT_NEXT_MESS ;
				break ;
			} else {
				sim800_current_state = STEP_GOOD_CONNECT ;
			}
		} break ;
		//-----------------------------------------------------------

		default : {
			sprintf( debug_buffer, "UPS! It`s DEFAULT step.\r\n" ) ;
			HAL_UART_Transmit(DEBUG_UART, (uint8_t *)debug_buffer, strlen(debug_buffer), 1000 );
			system_tik_u32 = HAL_GetTick();
			sim800_current_state = STEP_FIRST ;
		} break ;
	}	//	switch
} //	void
//**********************************************************************************************

/**
 * Call back function for release read SIM800 UART buffer.
 * @param NONE
 * @return NONE
 */
void Sim800_RxCallBack(void) {
    rx_buffer[rx_index++] = rx_data;

    if (SIM800.mqttServer.connect == 0) {
        if (strstr((char *) rx_buffer, "\r\n") != NULL && rx_index == 2) {
            rx_index = 0;
        } else if (strstr((char *) rx_buffer, "\r\n") != NULL) {
            memcpy(mqtt_buffer, rx_buffer, sizeof(rx_buffer));
            clearRxBuffer();
            if (strstr(mqtt_buffer, "DY CONNECT\r\n")) {
                SIM800.mqttServer.connect = 0;
            } else if (strstr(mqtt_buffer, "CONNECT\r\n")) {
                SIM800.mqttServer.connect = 1;
            }
        }
    }
    if (strstr((char *) rx_buffer, "CLOSED\r\n") || strstr((char *) rx_buffer, "ERROR\r\n")) {
        SIM800.mqttServer.connect = 0;
    }
    if (SIM800.mqttServer.connect == 1 && rx_data == 48) {
        mqtt_receive = 1;
    }
    if (mqtt_receive == 1) {
        mqtt_buffer[mqtt_index++] = rx_data;
        if (mqtt_index > 1 && mqtt_index - 1 > mqtt_buffer[1]) {
            MQTT_Receive((unsigned char *) mqtt_buffer);
            clearRxBuffer();
            clearMqttBuffer();
        }
        if (mqtt_index >= sizeof(mqtt_buffer)) {
            clearMqttBuffer();
        }
    }
    if (rx_index >= sizeof(mqtt_buffer)) {
        clearRxBuffer();
        clearMqttBuffer();
    }
    HAL_UART_Receive_IT(SIM800_UART, &rx_data, 1);
}

/**
 * Clear SIM800 UART RX buffer.
 * @param NONE
 * @return NONE
 */
void clearRxBuffer(void) {
    rx_index = 0;
    memset(rx_buffer, 0, sizeof(rx_buffer));
}

/**
 * Clear MQTT buffer.
 * @param NONE
 * @return NONE
 */
void clearMqttBuffer(void) {
    mqtt_receive = 0;
    mqtt_index = 0;
    memset(mqtt_buffer, 0, sizeof(mqtt_buffer));
}

/**
 * Send AT command to SIM800 over UART.
 * @param command the command to be used the send AT command
 * @param reply to be used to set the correct answer to the command
 * @param delay to be used to the set pause to the reply
 * @return error, 0 is OK
 */
//int SIM800_SendCommand(char *command, char *reply, uint16_t delay) {
//    HAL_UART_Transmit(SIM800_UART, (unsigned char *) command,
//                         (uint16_t) strlen(command));
//
//#if FREERTOS == 1
//    osDelay(delay);
//#else
//    HAL_Delay(delay);
//#endif
//
//    if (strstr(mqtt_buffer, reply) != NULL) {
//        clearRxBuffer();
//		sprintf( mqtt_uart_buff, " + %s", command ) ;
//		HAL_UART_Transmit(DEBUG_UART, (uint8_t *)mqtt_uart_buff, strlen(mqtt_uart_buff), 100);
//        return 0;
//    }
//    clearRxBuffer();
//    	sprintf( mqtt_uart_buff, " - %s" , command) ;
//		HAL_UART_Transmit(DEBUG_UART, (uint8_t *)mqtt_uart_buff, strlen(mqtt_uart_buff), 100);
//    return 1;
//}

/**
 * initialization SIM800.
 * @param NONE
 * @return error status, 0 - OK
 */
//int MQTT_Init(void) {
//	sprintf( mqtt_uart_buff, "MQTT init:\r\n") ;
//	HAL_UART_Transmit(DEBUG_UART, (uint8_t *)mqtt_uart_buff, strlen(mqtt_uart_buff), 100);
//
//    SIM800.mqttServer.connect = 0;
//    int error = 0;
//    char str[32] = {0};
//    HAL_UART_Receive_IT(SIM800_UART, &rx_data, 1);
//
//    SIM800_SendCommand("AT\r\n", "OK\r\n", CMD_DELAY);
//    SIM800_SendCommand("ATE0\r\n", "OK\r\n", CMD_DELAY);
//    error += SIM800_SendCommand("AT+CIPSHUT\r\n", "SHUT OK\r\n", CMD_DELAY);
//    error += SIM800_SendCommand("AT+CGATT=1\r\n", "OK\r\n", CMD_DELAY);
//    error += SIM800_SendCommand("AT+CIPMODE=1\r\n", "OK\r\n", CMD_DELAY);
//    error += SIM800_SendCommand("AT+COPS?\r\n", "", CMD_DELAY);
//    if (SIM800.sim.apn[0] && SIM800.sim.apn_user[0] && SIM800.sim.apn_pass[0]) {
//    	snprintf(str, sizeof(str), "AT+CSTT=\"%s\",\"%s\",\"%s\"\r\n", SIM800.sim.apn, SIM800.sim.apn_user,
//             SIM800.sim.apn_pass);
//    } else if (SIM800.sim.apn[0]) {
//        snprintf(str, sizeof(str), "AT+CSTT=\"%s\"\r\n", SIM800.sim.apn);
//    } else {
//    	error += 1;
//    	return error;
//    }
//    error += SIM800_SendCommand(str, "OK\r\n", CMD_DELAY);
//    error += SIM800_SendCommand("AT+CSTT?\r\n", "", CMD_DELAY);
//    error += SIM800_SendCommand("AT+CIICR\r\n", "OK\r\n", CMD_DELAY);
//    SIM800_SendCommand("AT+CIFSR\r\n", "", CMD_DELAY);
//
//	sprintf( mqtt_uart_buff, "MQTT errors=%d;\r\n " , error ) ;
//	HAL_UART_Transmit(DEBUG_UART, (uint8_t *)mqtt_uart_buff, strlen(mqtt_uart_buff), 100);
//
//    if ( (error + cmd_errors_i) == 0) {
//        MQTT_Connect();
//        return error;
//    } else {
//        return error;
//    }
//}

/**
 * Connect to MQTT server in Internet over TCP.
 * @param NONE
 * @return NONE
 */
//void MQTT_Connect(void) {
////	SIM800.mqttReceive.newEvent = 0;
////    SIM800.mqttServer.connect = 0;
////    char str[128] = {0};
////    unsigned char buf[128] = {0};
////    sprintf(str, "AT+CIPSTART=\"TCP\",\"%s\",%d\r\n", SIM800.mqttServer.host, SIM800.mqttServer.port);
////    SIM800_SendCommand(str, "OK\r\n", CMD_DELAY);
////#if FREERTOS == 1
////    osDelay(5000);
////#else
////    //HAL_Delay(5000);
////	for (int i = 0; i<5; i++) {
////		sprintf( mqtt_uart_buff, "  Delay3: %d\r", 4-i ) ;
////		HAL_UART_Transmit(DEBUG_UART, (uint8_t *)mqtt_uart_buff, strlen(mqtt_uart_buff), 100);
////		HAL_GPIO_TogglePin (LED_GPIO_Port, LED_Pin );
////		HAL_Delay(1000);
////	}
////	sprintf( mqtt_uart_buff, "\r\n") ;
////	HAL_UART_Transmit(DEBUG_UART, (uint8_t *)mqtt_uart_buff, strlen(mqtt_uart_buff), 100);
////#endif
////    if (SIM800.mqttServer.connect == 1) {
////        MQTTPacket_connectData datas = MQTTPacket_connectData_initializer;
////        datas.username.cstring = SIM800.mqttClient.username;
////        datas.password.cstring = SIM800.mqttClient.pass;
////        datas.clientID.cstring = SIM800.mqttClient.clientID;
////        datas.keepAliveInterval = SIM800.mqttClient.keepAliveInterval;
////        datas.cleansession = 1;
////        int mqtt_len = MQTTSerialize_connect(buf, sizeof(buf), &datas);
////        HAL_UART_Transmit(SIM800_UART, buf, mqtt_len);
////#if FREERTOS == 1
////        osDelay(5000);
////#else
////        //HAL_Delay(5000);
////    	for (int i = 0; i<5; i++) {
////    		sprintf( mqtt_uart_buff, "  Delay4: %d\r", 4-i ) ;
////    		HAL_UART_Transmit(DEBUG_UART, (uint8_t *)mqtt_uart_buff, strlen(mqtt_uart_buff), 100);
////    		HAL_GPIO_TogglePin (LED_GPIO_Port, LED_Pin );
////    		HAL_Delay(1000);
////    	}
////    	sprintf( mqtt_uart_buff, "\r\n") ;
////		HAL_UART_Transmit(DEBUG_UART, (uint8_t *)mqtt_uart_buff, strlen(mqtt_uart_buff), 100);
////#endif
////    }
//}

/**
 * Public on the MQTT broker of the message in a topic
 * @param topic to be used to the set topic
 * @param payload to be used to the set message for topic
 * @return NONE
 */
void MQTT_Pub(char *topic, char *payload) {
    unsigned char buffer_to_send[256] = {0};

    MQTTString topicString = MQTTString_initializer;
    topicString.cstring = topic;

    int mqtt_len = MQTTSerialize_publish(buffer_to_send, sizeof(buffer_to_send), 0, 0, 0, 0,
                                         topicString, (unsigned char *) payload, (int) strlen(payload));
    //HAL_UART_Transmit(SIM800_UART, buffer_to_send, mqtt_len);
    HAL_UART_Transmit( SIM800_UART, buffer_to_send, mqtt_len, 1000);
    //HAL_Delay(100);
}

/**
 * Send a PINGREQ to the MQTT broker (active session)
 * @param NONE
 * @return NONE
 */
void MQTT_PingReq(void) {
    unsigned char buf[16] = {0};

    int mqtt_len = MQTTSerialize_pingreq(buf, sizeof(buf));
    HAL_UART_Transmit(SIM800_UART, buf, mqtt_len, 1000 );
}

/**
 * Subscribe on the MQTT broker of the message in a topic
 * @param topic to be used to the set topic
 * @return NONE
 */
void MQTT_Sub(char *topic) {
    unsigned char buffer_to_send[256] = {0};

    MQTTString topicString = MQTTString_initializer;
    topicString.cstring = topic;

    int mqtt_len = MQTTSerialize_subscribe(buffer_to_send, sizeof(buffer_to_send), 0, 1, 1,
                                           &topicString, 0);
    HAL_UART_Transmit(SIM800_UART, buffer_to_send, mqtt_len, 1000 );
//#if FREERTOS == 1
//    osDelay(5000);
//#else
//    HAL_Delay(5000);
//#endif
}

/**
 * Receive message from MQTT broker
 * @param receive mqtt bufer
 * @return NONE
 */
void MQTT_Receive(unsigned char *buf) {
    memset(SIM800.mqttReceive.topic, 0, sizeof(SIM800.mqttReceive.topic));
    memset(SIM800.mqttReceive.payload, 0, sizeof(SIM800.mqttReceive.payload));
    MQTTString receivedTopic;
    unsigned char *payload;
    MQTTDeserialize_publish(&SIM800.mqttReceive.dup, &SIM800.mqttReceive.qos, &SIM800.mqttReceive.retained,
                            &SIM800.mqttReceive.msgId,
                            &receivedTopic, &payload, &SIM800.mqttReceive.payloadLen, buf,
                            sizeof(buf));
    memcpy(SIM800.mqttReceive.topic, receivedTopic.lenstring.data, receivedTopic.lenstring.len);
    SIM800.mqttReceive.topicLen = receivedTopic.lenstring.len;
    memcpy(SIM800.mqttReceive.payload, payload, SIM800.mqttReceive.payloadLen);
    SIM800.mqttReceive.newEvent = 1;
}
