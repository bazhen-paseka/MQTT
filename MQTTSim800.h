/*
 * MQTTSim800.h
 *
 *  Created on: Jan 4, 2020
 *      Author: Bulanov Konstantin
 *
 *  Contact information
 *  -------------------
 *
 * e-mail   :   leech001@gmail.com
 * telegram :   https://t.me/leech001
 *
 *
 */

/*
 * -----------------------------------------------------------------------------------------------------------------------------------------------
           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2020 Bulanov Konstantin <leech001@gmail.com>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.

  MQTT packet https://github.com/eclipse/paho.mqtt.embedded-c/tree/master/MQTTPacket
 * ------------------------------------------------------------------------------------------------------------------------------------------------
*/

#ifndef MQTT_SIM800_H_INCLUDED
#define MQTT_SIM800_H_INCLUDED
//**************************************************************

	#include <main.h>
	#include "Local_config.h"
//**************************************************************

// === CONFIG ===
	#define DEBUG_UART		&huart1
	#define SIM800_UART 	&huart2
	#define FREERTOS    	0

	#define MUX_DELAY   	3000
	#define GSM_DELAY   	10000

	#define CMD_DELAY   	2000
	#define CONNECT_DELAY	5000
//**************************************************************

typedef struct {
    char *apn;
    char *apn_user;
    char *apn_pass;
} sim_t;

typedef struct {
    char 		*host;
    uint16_t 	port;
    uint8_t 	connect;
} mqttServer_t;

typedef struct {
    char 			*username;
    char 			*pass;
    char 			*clientID;
    unsigned short 	keepAliveInterval;
} mqttClient_t;

typedef struct {
    uint8_t 		newEvent;
    unsigned char 	dup;
    int 			qos;
    unsigned char 	retained;
    unsigned short 	msgId;
    unsigned char 	payload[64];
    int 			payloadLen;
    unsigned char 	topic[64];
    int 			topicLen;
} mqttReceive_t;

typedef struct {
    sim_t sim;
    mqttServer_t 	mqttServer;
    mqttClient_t 	mqttClient;
    mqttReceive_t 	mqttReceive;
} SIM800_t;
//**************************************************************

void RF_sim800_Init		(void) ;
void RF_sim800_Main		(void) ;
void Sim800_RxCallBack	(void) ;
void clearRxBuffer		(void) ;
void clearMqttBuffer	(void) ;
void MQTT_PingReq		(void) ;
void MQTT_Sub			(char *topic);
void MQTT_Receive		(unsigned char *buf);
void MQTT_Pub			(char *topic, char *payload);
//**************************************************************

#endif /* MQTT_SIM800_H_INCLUDED */
